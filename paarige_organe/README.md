# paarige_organe.csv

## Zweck

Datei mit einer Zuordnung von ICD-10 Codes (ab ICD-10 GM 2012) zu den Auswahlmöglichkeiten für die "Seitenlokalisation" von Diagnosen solider Tumoren gemäß Basisdatensatz von ADT/GEKID (ab Version 1).

## Erläuterung der Spalten und Datensätze

Diagnosen, die sich mittels ICD-10 Code und ggf. ICD-O-3 Histologie keinem der enthaltenen Datensätze zuordnen lassen, betreffen demgemäß kein paariges Organ und sind folglich automatisch mit dem Code "T - Trifft nicht zu" für die Seitenlokalisation zu belegen.
Ist ein Prefix für den ICD-10 Code mehrfach vorhanden, so ist entsprechend das RegEx-Pattern für den Histologie-Code zu prüfen. Die Zurodnungen des Datensatzes mit dem passenden Pattern ist zu verwenden oder andernfalls jener ohne ein Histologie-RegEx-Pattern.

### Icd10Prefix
Teil des ICD-10 Codes (ICD-10 GM) oder bereits der vollständige Code, für den die zugeordneten Seitenlokalisationen (R/L/B/X) gelten. Der Begiff Präfix wurde gewählt, weil Code grundsätzlich mit folgendem Wildcard verstanden werden kann. Also bspw: `C50` entspricht dem RegEx-Pattern `C50\..*`.

### IcdO3HistologieRegExPattern
Ein RegEx-Pattern, falls die Zeile nur für ausgewählte ICD-O-3 Histologiecodes in Verbindung mit den angegebenen ICD-10 Codes gültig ist.
Bspw. ist für das Retinoblastom, mit dem ICD-10 Code `C69.2` nur dann der Code "b - beidseitig" gültig, wenn die entsprechende Histologie in den Bereich `9510/3` bis `9514/3` fällt.

### Spalten R/L/B/X

Diese vier Spalten geben die Gültigkeit des jeweiligen Codes der Seitenlokalisation, also "R - rechts", "L - links", "B - beidseitig" und "X - unbekannt" wieder.
