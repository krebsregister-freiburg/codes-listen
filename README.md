# Codes und Listen

Listen genereller Codes, die für die Verwendung im klinischen Krebsregister relevant sind sowie bei der Meldung an das Landeskrebsregister BW Verwendung finden.

Wie dem beigefügten Dokument `LICENSE` zu entnehmen ist, sind die Inhalte dieses Repositories frei von jeglichem Vorbehalt von Verwertungsrechten und damit frei zu verwenden, privat oder kommerziell.