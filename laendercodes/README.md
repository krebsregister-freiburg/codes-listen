# laendercodes.csv

## Zweck
Datei mit Auflistung aller im Klinischen Krebsregister des Tumorzentrums Freiburg verwendeten Ländercodes.

## Erläuterung der Spalten

### Land
Name des Landes

### ISO-3166-1-ALPHA-2
Zweistelliger alphanumerischer Code des Landes nach ISO-3166-1 

### ISO-3166-1-ALPHA-3
Dreistelliger alphanumerischer Code des Landes nach ISO-3166-1 

### DESTATIS_BEV
Länder-Schlüssel des Statistischen Bundesamtes Deutschland

### KFZ
Länder-Code, wie er üblicherweise für KFZ-Kennzweichen verwendet wird (kein einheitlicher Standard)

### LKRBW_CODE
Ländercode, den die Vertrauensstelle des Landeskrebsregisters Baden-Württemberg verwendet (soweit vorhanden)

### ValidFromYear
Falls zutreffend, das Jahr, ab dem der Eintrag gültig ist/war (bspw. Tschechische Republik ab 1992)

### ValidToYear
Falls zutreffend, das Jahr, bis zu dem der Eintrag gültig war (bspw. Jugoslawien bis einschließlich 1992)